package com.sg.spark.jobs;

import static com.sg.constants.Constants.ALARM;
import static com.sg.constants.Constants.ALARMNAME;
import static com.sg.constants.Constants.AWS_ACCOUNT;
import static com.sg.constants.Constants.COMMA;
import static com.sg.constants.Constants.FULL;
import static com.sg.constants.Constants.JSON;
import static com.sg.constants.Constants.PIPE;
import static com.sg.constants.Constants.PROCESS_CREATED_DATE;
import static com.sg.constants.Constants.PROCESS_UPDATED_DATE;
import static com.sg.constants.Constants.TAB;
import static com.sg.constants.Constants.UPSERT;
import static com.sg.constants.Constants.XML;
import static com.sg.constants.Constants.YES;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sg.spark.core.AbstractSparkBatchJob;
import com.sg.udf.functions.DataFilterByDate;
import com.sg.util.DateUtil;
import com.sg.util.S3Util;
import com.sg.util.SNSNotification;
import com.sg.util.SNSUtil;

import scala.collection.Seq;

@Component("sparkDataIngestJob")
public class Usecase1 extends AbstractSparkBatchJob implements Serializable {
	String columRegex = "[ ,;{}()\n\t=]";
	Pattern renameColumPattern = Pattern.compile(columRegex);
	@Value("${data.input.location:}")
	private String dataInputLoc;

	@Value("${data.output.location:}")
	private String dataOutputLoc;

	@Value("${data.temp.location:}")
	private String dataTempLoc;

	@Value("${data.delimeter:}")
	private String dataDelimeter;

	@Value("${data.load.type:}")
	private String loadType;

	@Value("${data.join.colums.comma.separated:}")
	private String dataJoinColums;

	@Value("${data.colums.actual.names:}")
	private String dataColumActualNames;

	@Value("${data.rename.colums:}")
	private String dataRenameColums;

	@Value("${data.xml.rowTag:}")
	private String xmlRowTag;

	@Value("${sns.alert.address:}")
	private String snsAlert;
	
	@Value("${data.schema.location:}")
	private String dataSchemaLocation;
	
	@Value("${isMultilineJson:}")
	private String isMultilineJson;
	

	@Autowired
	private DataFilterByDate dataFilterByDate;



	@Override
	public void exec() {
		try {
			SparkSession spark = this.getSparkSession();

			// read data frame
			//etl logic
			//write all are common methds below in this class or we can move those to common util methods to use other jobs



		} catch (Exception e) {
			e.printStackTrace();
			SNSNotification notification=new SNSNotification();
			notification.setAlarmName(ALARMNAME);
			notification.setAwsAccount(AWS_ACCOUNT);
			notification.setNewStateValue(ALARM);
			notification.setAlarmDescription(e.getMessage());	
			ObjectMapper mapper = new ObjectMapper(); 
			String notificationMessage = null;
			try {
				notificationMessage = mapper.writeValueAsString(notification);
			} catch (JsonProcessingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			SNSUtil.sendSNSMessage(snsAlert,
					notificationMessage);
			throw new RuntimeException("Spark Job Failed"+e.getMessage());
			}

	}
	
	

	private void writeData(Dataset<Row> data, Boolean isOverwrite, String location) {
		if (isOverwrite) {
			if (location.startsWith("s3://")) {
				data.write().option("compression", "snappy").option("header", "true").format("parquet")
						.mode(SaveMode.Overwrite).save(location);
			} else {
				data.show();;
				data.printSchema();
				data.write().option("header", "true").mode(SaveMode.Overwrite).save(location);
			}
		} else {
			if (location.startsWith("s3://")) {
				data.write().option("compression", "snappy").option("header", "true").format("parquet")
						.mode(SaveMode.Append).save(location);
			} else {
				data.write().option("header", "true").mode(SaveMode.Append).save(location);


			}
		}
	}
	
	private void writeSchemaTos3(Dataset<Row> data) throws FileNotFoundException {
		String jsonSchema=data.schema().prettyJson();
		
		if (dataSchemaLocation.startsWith("s3://")) {
			S3Util s3Util=S3Util.getInstance();
			String bucket=s3Util.getBucket(dataSchemaLocation);
			String key=s3Util.getKey(dataSchemaLocation);
			s3Util.upload(bucket, key, IOUtils.toInputStream(jsonSchema), jsonSchema.length());

			
		} else {
			PrintWriter out = new PrintWriter("schema.txt");

		}
	}

	private Seq<String> getColumSeq(String commaSeparatedString) {
		String[] stringrray = commaSeparatedString.split(",");
		List<String> columsList = new ArrayList<String>();
		for (String string2 : stringrray) {
			columsList.add(string2.replaceAll(columRegex, ""));
		}
		Seq<String> seqOfColums = scala.collection.JavaConverters.collectionAsScalaIterableConverter(columsList)
				.asScala().toSeq();
		return seqOfColums;
	}

	private static Dataset<Row> loadHistorical(String history, SparkSession spark, StructType schema) {
		Dataset<Row> historicalData = null;
		try {
			historicalData = spark.read().schema(schema).parquet(history);
			return historicalData;

		} catch (Exception e) {
			return null;
		}
	}

	private static readfile(String inputloc,boolean inferschema,String delimeter){
		return spark.read().option("mode", "DROPMALFORMED").option("header", "true")
				.option("delimiter", delimeter).option("quote", "\"").option("encoding", "UTF-8")
				.option("inferSchema", inferschema).csv(inputloc);
	}

}
