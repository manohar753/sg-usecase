package com.sg.spark.jobs;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

public class IncrementalPOC {

	public static void main(String[] args) {
		SparkSession spark = SparkSession.builder().appName("Java Spark SQL Example").getOrCreate();
		spark.read().parquet("/Users/manohar.reddy/test/output").show();


//		String history = "s3://kilimanjaro-test-datalake/newtest201807/sparkdata/demo/output/stage/";
//		String refreshloc = "s3://kilimanjaro-test-datalake/newtest201807/sparkdata/demo/output/refresh/";
//		String tempoutput = "s3://kilimanjaro-test-datalake/newtest201807/sparkdata/demo/output/temp/";
//
//		String input = args[0];
//		String isUpsert = args[1]; /// upsertaaaaaaaaaaaaaaaaaa
//
//		// dispatching_base_number,date,active_vehicles,trips
//		StructType schema = new StructType().add("IP", DataTypes.StringType).add("Unique_Value", DataTypes.StringType)
//				.add("ListenerId", DataTypes.StringType).add("PlayerId", DataTypes.StringType)
//				.add("UserAgent", DataTypes.StringType).add("Device", DataTypes.StringType)
//				.add("StartDateTime", DataTypes.StringType).add("EndDateTime", DataTypes.StringType)
//				.add("Start_Date", DataTypes.StringType).add("Start_Time", DataTypes.StringType)
//				.add("End_Date", DataTypes.StringType).add("End_Time", DataTypes.StringType)
//				.add("Stream", DataTypes.StringType).add("Station", DataTypes.StringType);
//
//		Dataset<Row> deltaDF = spark.read().option("mode", "DROPMALFORMED").schema(schema).csv(input);
//		deltaDF.show();
//
//		Dataset<Row> hdf = loadHistorical(history, spark);
//		if (hdf != null & isUpsert.equalsIgnoreCase("upsert")) {
//			hdf.show();
//			System.out.println("hitory");
//
//			System.out.println("todays");
//
//			Dataset<Row> historyIds = hdf.select("Unique_Value", "StartDateTime", "Station")
//					.withColumnRenamed("Unique_Value", "UniqueValueT")
//					.withColumnRenamed("StartDateTime", "StartDateTimeT").withColumnRenamed("Station", "StationT");
//
//			Dataset<Row> updatedRows = historyIds
//					.join(deltaDF,
//							historyIds.col("UniqueValueT").equalTo(deltaDF.col("Unique_Value"))
//									.and(historyIds.col("StartDateTimeT").equalTo(deltaDF.col("StartDateTime")))
//									.and(historyIds.col("StationT").equalTo(deltaDF.col("Station"))),
//							"inner")
//					.selectExpr(deltaDF.columns());
//			updatedRows.show();
//			System.out.println("udates");
//
//			Dataset<Row> newRows = deltaDF
//					.join(hdf,
//							hdf.col("Unique_Value").equalTo(deltaDF.col("Unique_Value"))
//									.and(hdf.col("StartDateTime").equalTo(deltaDF.col("StartDateTime")))
//									.and(hdf.col("Station").equalTo(deltaDF.col("Station"))),
//							"leftanti")
//					.selectExpr(deltaDF.columns());
//
//			newRows.show();
//			System.out.println("news");
//
//			Dataset<Row> nonupdateRows = hdf
//					.join(deltaDF,
//							hdf.col("Unique_Value").equalTo(deltaDF.col("Unique_Value"))
//									.and(hdf.col("StartDateTime").equalTo(deltaDF.col("StartDateTime")))
//									.and(hdf.col("Station").equalTo(deltaDF.col("Station"))),
//							"leftanti")
//					.selectExpr(hdf.columns());
//
//			nonupdateRows.show();
//			System.out.println("non updates");
//
//			updatedRows.union(nonupdateRows).union(newRows).write().option("compression", "snappy")
//					.option("header", "true").format("parquet").mode(SaveMode.Overwrite).save(tempoutput);
//
//		} else {
//			if (isUpsert.equalsIgnoreCase("upsert")) {
//				deltaDF.write().option("compression", "snappy").option("header", "true").format("parquet")
//						.mode(SaveMode.Overwrite).save(tempoutput);
//			} else {
//				deltaDF.write().option("compression", "snappy").option("header", "true").format("parquet")
//						.mode(SaveMode.Append).save(refreshloc);
//			}
//		}

	}

	private static Dataset<Row> loadHistorical(String history, SparkSession spark) {
		Dataset<Row> hdf = null;
		try {
			hdf = spark.read().parquet(history);
			return hdf;

		} catch (Exception e) {
			return null;
		}
	}

}
