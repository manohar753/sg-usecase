package com.sg.spark.jobs;

import java.util.Properties;


public enum SparkConfigKeys {

    DP_SPARK_JOB_ID("dp.job.id", "not-defined"),
    DP_SPARK_JOB_TEMPDIR("dp.spark.job.temp.dir", "/tmp/"),
    DP_OUTPUT_PARTITIONER("dp.output.partitioner", "voidPartition");

    private String key;
    private Object defaultValue;

    /**
     * Constructor for SparkConfigKeys
     *
     * @param key Configuration key
     * @param defaultValue Default value for the configuration
     */
    SparkConfigKeys(final String key, Object defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    /**
     * Constructor for SparkConfigKeys
     *
     * @param key Configuration key
     */
    SparkConfigKeys(final String key) {
        this(key, null);
    }

    @Override
    public String toString() {
        return key;
    }

    /**
     * return String value
     *
     * @param props
     * @return String
     */
    public String stringVal(Properties props) {

        if (org.springframework.util.StringUtils.isEmpty(props.getProperty(key))) {
            return (String) defaultValue;
        } else {
            return props.getProperty(key);
        }
    }

    /**
     * return int value
     *
     * @param props
     * @return int
     */
    public int intVal(Properties props) {
        return Integer.parseInt(this.stringVal(props));
    }

    /**
     * return long value
     *
     * @param props
     * @return long
     */
    public long longVal(Properties props) {
        return Long.parseLong(this.stringVal(props));
    }

    /**
     * return boolean value
     *
     * @param props
     *
     * @return boolean
     */
    public boolean boolVal(Properties props) {
        return Boolean.parseBoolean(this.stringVal(props));
    }

    public String defaultVal(){
        return (String)defaultValue;
    }
}
