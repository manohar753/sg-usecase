package com.sg.spark.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * This is useful for other non spring engines (like spark) to access spring beans
 */
public class Spring {

    static Spring spring;

    private  ClassPathXmlApplicationContext ctx;

    private static final Logger logger = LoggerFactory.getLogger(Spring.class);

    private Spring(String jobId){

        logger.info("Initializing spring context [{}] at {} ", jobId, sgSystem.getName());
        java.lang.System.setProperty("dp.job.id", jobId);
        ctx =  new ClassPathXmlApplicationContext("spring/*.xml");
    }


    public static Spring bridge(String jobId){
        if(spring == null){
            spring = new Spring(jobId);
        }
        return spring;
    }

    public Object get(String name){
        return ctx.getBean(name);
    }

    public <T> T get(String name, Class<T> c){
        return (T)ctx.getBean(name);
    }

    public <T> T get(Class<T> c){
        return (T)ctx.getBean(c);
    }
}
