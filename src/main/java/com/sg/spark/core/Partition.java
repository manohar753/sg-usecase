package com.sg.spark.core;


public interface Partition {

    /**
     * Implement how ever you want
     *
     * @param prefix
     * @param value
     * @param value
     * @return
     */
    public String getUri(String prefix, String value, String valueFormat);

}
