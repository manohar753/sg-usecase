package com.sg.spark.core;

import java.io.IOException;
import java.util.Properties;

import org.apache.hadoop.mapreduce.Job;
import org.apache.log4j.PropertyConfigurator;
import org.apache.spark.SparkConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sg.spark.jobs.SparkConfigKeys;


public abstract class AbstractSparkJob {

    @Autowired
    private SparkConf sparkConf;

    @Autowired
    private Properties config;

    private String jobId;

    private Job job;

    private Partition partition;

//    private static final String logConf = "/log-conf/log4j.properties";

    private static final Logger logger = LoggerFactory.getLogger(AbstractSparkJob.class);

    /**
     * Initializes all spark job properties before execution. This class may
     * also be used to initialize the source.
     */
    public void init() {

//        this.setCustomLogger();

        this.loadProps();

        try {
                this.job = Job.getInstance();
        } catch (IOException ex) {
            logger.error("Unable to get Job instance.");
            throw new RuntimeException("Unable to get Job instance.");
        }
        //setting partitioner bean
        if (partition == null) {
            partition = Spring.bridge(getJobId()).get(SparkConfigKeys.DP_OUTPUT_PARTITIONER.stringVal(getConfig()), Partition.class);
        }


    }
    
    /**
     * Defines the Spark Job execution flow
     */
    public abstract void exec();

    /**
     * Executes all post-processing tasks
     */
    public abstract void finish();

    /**
     * Runs the spark job
     */
    public void run() {
        logger.info("Starting spark job {} at {} ", jobId, sgSystem.getName());
        init();
        exec();
        finish();
        logger.info("Spark job {} finished at {} ", jobId, sgSystem.getName());
    }

    /**
     * Returns the SparkConf for this spark job
     *
     * @return {@link SparkConf}
     */
    public SparkConf getSparkConf() {
        return sparkConf;
    }

    /**
     * Returns the properties for this spark job
     *
     * @return {@link Properties}
     */
    public Properties getConfig() {
        return config;
    }

    /**
     * Loads all Spark properties from the config into the SparkConf for this
     * spark job
     */
    private void loadProps() {

        for (String prop : config.stringPropertyNames()) {
            if (prop.startsWith("spark.")) {
                sparkConf.set(prop, config.getProperty(prop));
                logger.info("Setting sparkConf {}={}", prop, config.getProperty(prop));
            }
        }

    }

    /**
     * changing log conf file
     */
//    private void setCustomLogger(){
//        PropertyConfigurator.configure(getClass().getResourceAsStream(getLogConf()));
//    }

    /**
     * @param jobId
     * @return
     */
    public AbstractSparkJob setJobId(String jobId) {
        this.jobId = jobId;
        return this;
    }

    /**
     * Get Job Id
     *
     * @return
     */
    public String getJobId() {
        return jobId;
    }

    /**
     * Returns Job instance
     *
     * @return
     */
    public Job getJob() {
        return this.job;
    }

    /**
     * Returns the partitioner
     * 
     * @return 
     */
    public Partition getPartition() {
        return partition;
    }

    /**
     * Returns temporary directory specified in the config
     * 
     * @return 
     */
    public String getTempDir() {
        return SparkConfigKeys.DP_SPARK_JOB_TEMPDIR.stringVal(this.getConfig()) + jobId + "/";
    }

    /**
     * setting log conf
     * 
     * @return
     */
//    public String getLogConf() {
//        return logConf;
//    }


}
