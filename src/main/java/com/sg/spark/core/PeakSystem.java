package com.sg.spark.core;

import java.net.InetAddress;


public class sgSystem {


    private static transient String name;

    /**
     * get system name
     * @return
     */
    public static final String getName(){
        try{
            if(name == null){
                name = InetAddress.getLocalHost().getHostName();
            }
            return name;
        }catch(Exception e){
            return "unknown";
        }

    }


    /**
     * get host address (ip)
     * @return
     */
    public static final String getIP(){
        try{
            return InetAddress.getLocalHost().getHostAddress();
        }catch(Exception e){
            return "unknown";
        }

    }
}
