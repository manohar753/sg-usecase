package com.sg.spark.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.*;
import java.lang.System;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * System gets higher priority all the time, that means any propeties in files can be ovverriden by setting system properties
 * <p>
 *     For example:  application.properties has
 *          company = sg
 *     Then you can start program by java -Dcompany=sg  -jar abc.jar - which will replace "company" to "sg"
 * </p>
 */
public class PropertiesFactoryBean extends org.springframework.beans.factory.config.PropertiesFactoryBean {

    private static final Logger logger = LoggerFactory.getLogger(PropertiesFactoryBean.class);

    /**
     * System gets higher priority all the time, that means any propeties in files can be ovverriden by setting system properties
     *
     * If you do not want system property to take precedence then just prefix your property with "final.", example
     *
     * final.spark.core.memrory=8g means no way you can override it
     *
     * @return
     * @throws IOException
     */
    @Override
    protected Properties createProperties() throws IOException {
        logger.info("Merging spring and java system properties");
        Properties properties = this.mergeProperties();
        properties.putAll(System.getProperties());
        //now see if there any properties not in system but in app properties files with prefix "final."
        for(Object keyObj : properties.keySet().toArray()){
            String key = keyObj.toString();
            if(key.startsWith("final.")){
                properties.setProperty(key.substring(6), properties.getProperty(key));
            }
        }
        return properties;
    }
}
