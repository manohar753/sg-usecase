package com.sg.spark.core;

import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;


public abstract class AbstractSparkBatchJob extends AbstractSparkJob {

    private JavaSparkContext sparkCtx;
    private SparkSession sparkSession ;

    @Override
    public void init() {
        super.init();
        sparkCtx = new JavaSparkContext(this.getSparkConf());
        sparkSession = SparkSession
				.builder()
				.config(this.getSparkConf())
				.getOrCreate();
    }

    @Override
    public void finish() {
        sparkCtx.stop();
        sparkSession.stop();
    }

    /**
     * Returns the JavaSparkContext
     *
     * @return {@link JavaSparkContext}
     */
    public JavaSparkContext getSparkContext() {
        return sparkCtx;
    }
    
    /**
     * Returns the SparkSession
     *
     * @return {@link JavaSparkContext}
     */
    public SparkSession getSparkSession() {
        return sparkSession;
    }

}
