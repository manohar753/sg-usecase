package com.sg.spark.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sg.spark.jobs.SparkConfigKeys;

/**
 *
 * Start point of spark application
 *
 * Example: spark-submit --conf
 * "spark.executor.extraJavaOptions=-Ddata.input.location=s3://test/
 * -Dcompany=sg" --conf
 * "spark.driver.extraJavaOptions=-Ddata.input.location=s3://test/
 * -Dcompany=sg" --class com.sg.core.spark.SparkApp
 * /tmp/data-stream-1.0.0-SNAPSHOT.jar oneid-log-preprocessor-dev
 *
 */
@ComponentScan
public class SparkApp {

	private static final Logger logger = LoggerFactory.getLogger(SparkApp.class);

	private static void launch(String[] args) {

		if (args.length == 0) {
			throw new RuntimeException(
					"Job id is mandatory as command line argument. [arg1=jobid, example tenant1-preprocessor-dev]");
		} else {
			String jobId = args[0];
			// setting system props

			java.lang.System.setProperty(SparkConfigKeys.DP_SPARK_JOB_ID.toString(), jobId);
			ClassPathXmlApplicationContext ctx2 = new ClassPathXmlApplicationContext("spring/*.xml");
			ctx2.start();
		}

	}

	/**
	 * Main execution for Spark Jobs. This class is responsible for initializing the
	 * Spring Context and kicking off the spark job
	 *
	 * How to run spark-submit com.sg.core.spark.SparkApp <job-id>
	 * <environment> Example: spark-submit com.sg.core.spark.SparkApp
	 * cache-fuse dev
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		logger.info("Starting spark app main at {} ", sgSystem.getName());
		launch(args);
		 logger.info("Finished spark app main at {} ",
		 sgSystem.getName());
	}

}
