package com.sg.spark.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.stereotype.Component;

/**
 * Just launch the job as per command line argument id
 */
@Component
public class SparkAppLauncher implements ApplicationContextAware, ApplicationListener<ContextStartedEvent> {

    @Value("${dp.job.id}")
    String jobId;

    @Value("${dp.job.bean}")
    String jobBean;

    ApplicationContext applicationContext;

    private static final Logger logger = LoggerFactory.getLogger(SparkAppLauncher.class);


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void onApplicationEvent(ContextStartedEvent event) {
        logger.info("Invoking {} ", jobBean);
        AbstractSparkJob job = applicationContext.getBean(jobBean, AbstractSparkJob.class).setJobId(jobId);
        job.setJobId(jobId);
        job.run();
    }
}
