package com.sg.udf.functions;

import static com.sg.constants.Constants.PROCESS_UPDATED_DATE;
import static com.sg.constants.Constants.YYYYMMDD;
import static com.sg.util.DateUtil.getPrevNdays;
import static com.sg.util.DateUtil.getToday;
import static com.sg.util.DateUtil.isDateInRange;

import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Row;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;






@Component
public class UseCase2Udf implements FilterFunction<Row> {

    @Value("${dp.searchString:"Banque"}")
    private String searchString;

    @Value("${dp.colName:"Banque"}")
    private String colName;


    @Override
    public boolean call(Row value) throws Exception {
        int colIndex = value.schema().fieldIndex(colName);
        String colValue = value.getString(colIndex);
       return  colValue.equalsIgnoreCase(searchString)
    }
}
