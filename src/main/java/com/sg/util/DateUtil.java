package com.sg.util;

import static com.sg.constants.Constants.YYYYMMDD;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



final public class DateUtil {
    
    private static String defaultTimeZone="America/New_York";

    private static final Logger logger = LoggerFactory.getLogger(DateUtil.class);

    private DateUtil() {

    }

    public static String getDateHyphenatedString(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("'%tY-%tm-%td'", cal,cal,cal);
    }

    public static String getDateUnderscoredString(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("'%tY_%tm_%td'", cal,cal,cal);
    }

    public static String getDateHyphenatedStringWithoutQuotes(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("%tY-%tm-%td", cal,cal,cal);
    }

    public static String getDateUnderscoredStringWithoutQuotes(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("%tY_%tm_%td", cal,cal,cal);
    }

    public static String getDateAndedString(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("year=%tY and month=%tm and date=%td", cal,cal,cal);
    }

    public static String getDayAndedString(final Date baseDay, final int offset) {
        final Calendar cal = getOffsetedDate(baseDay, offset);
        return String.format("year=%tY and month=%tm and day=%td", cal,cal,cal);
    }

    public static String getDateAndedStringWithPrefix(final Date baseDay, final int offset, final String prefix) {
        final Calendar cal = getOffsetedDate(baseDay, offset);
        return String.format(prefix +".year=%tY and " + prefix + ".month=%tm and " + prefix + ".date=%td", cal,cal,cal);
    }

    public static String getDateAndedStringForYesterdayTodayAndTomorrow(final Date baseDate, final int offset) {
    	 final Calendar today = getOffsetedDate(baseDate, offset);
    	 final Calendar yesterday = getOffsetedDate(today.getTime(), -1);
    	 final Calendar tomorrow = getOffsetedDate(today.getTime(), 1);
    	 return "("
    			 	+ "(" + getDateAndedString(yesterday.getTime(), offset) + ")"
    			 	+ " OR "
    			 	+ "(" + getDateAndedString(today.getTime(), offset) + ")"
    			 	+ " OR "
    			 	+ "(" + getDateAndedString(tomorrow.getTime(), offset) + ")"
				+ ")";
    }

    public static String getDateAndedStringForTodayAndTomorrow(final Date baseDate, final int offset) {
        final Calendar today = getOffsetedDate(baseDate, offset);
        final Calendar tomorrow = getOffsetedDate(today.getTime(), 1);
        return "("
                + "(" + getDateAndedString(today.getTime(), offset) + ")"
                + " OR "
                + "(" + getDateAndedString(tomorrow.getTime(), offset) + ")"
                + ")";
    }

    public static String getDateAndedStringWithExtraMonth(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        final Calendar nextDay = getOffsetedDate(cal.getTime(), 1);
        return String.format("year=%tY and month=%tm" + getExtraMonth(baseDate, offset) + " and (date=%td or date=%td)", cal,cal,cal,nextDay);
    }

    public static String getYearString(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("%tY", cal);
    }

    public static String getMonthString(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        String month = Integer.toString(cal.get(Calendar.MONTH) + 1);
        return String.format("%tm", cal);
    }

    public static String getDateString(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("%td", cal);
    }

    public static Calendar getOffsetedDate(final Date baseDate, final int offset) {
        final Calendar cal = Calendar.getInstance();
        if (baseDate != null) {
            cal.setTime(baseDate);
        }
        cal.add(Calendar.DATE, offset);
        return cal;
    }

    public static String getDatePartitionStringForS3(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("year=%tY/month=%tm/date=%td", cal,cal,cal);
    }
    
    public static String getDayPartitionStringForS3(final Date baseDate, final int offset) {
        final Calendar cal = getOffsetedDate(baseDate, offset);
        return String.format("year=%tY/month=%tm/day=%td", cal,cal,cal);
    }
    
    public static String getExtraMonth (final Date baseDate, final int offset) {
        return DateUtil.getDateString(baseDate, offset+1).equalsIgnoreCase("01") ? " and month=" + DateUtil.getMonthString(baseDate, offset+1) : "";
    }

  

    public static String getFormatedDateString(Calendar cal) {
    	if (cal == null) {
    		return "";
    	}
    	return getFormatedDateString(cal.getTime());
    }

    public static String getFormatedDateString(Date date) {
    	if (date == null) {
    		return "";
    	}
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
    
    public static String getFormatedDateStringWithTime(Date date) {
    	if (date == null) {
    		return "";
    	}
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 'T' HH:mm:ss z");
        return sdf.format(date);
    }
    public static long dateDiff (final Date startDate, final Date endDate) {
        final long diff = endDate.getTime() - startDate.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
    
    public static String getToday() {
    			
    	return java.time.format.DateTimeFormatter.ofPattern("yyyy-MM-dd 'T' HH:mm:ss z").format(ZonedDateTime.now(ZoneId.of(defaultTimeZone)))  ;
    	}
    
    public static String getYesterday() {
    	ZonedDateTime date = ZonedDateTime.now(ZoneId.of(defaultTimeZone));
    	date = date.plusDays(-1);
    	return     	java.time.format.DateTimeFormatter.ofPattern("yyyy/MM/dd ").format(date)  ;

    }
    public static String getPrevNdays(int n) {
    	ZonedDateTime date = ZonedDateTime.now(ZoneId.of(defaultTimeZone));
    	date = date.plusDays(-n);
    	return     	java.time.format.DateTimeFormatter.ofPattern("yyyy/MM/dd ").format(date)  ;

    }

    //check to see if the Given Date is in Range
    public static boolean isDateInRange(String dateFrom, String dateTo, String datePattern, String givenDate) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(datePattern);
        try {
            Date fromDate = simpleDateFormat.parse(dateFrom);
            Date toDate = simpleDateFormat.parse(dateTo);
            Date dateToCopare = simpleDateFormat.parse(givenDate);


            if (dateToCopare.after(fromDate) && dateToCopare.before(toDate)) {
                return true;
            }
        } catch (Exception e){
            logger.error(e.getMessage());
        }
        return false;
    }
    
    public static void main(String[] args) {
		DateUtil du=new DateUtil();
		System.out.println(du.getPrevNdays(-2));
		ZonedDateTime date = ZonedDateTime.now();

		System.out.println(java.time.format.DateTimeFormatter.ofPattern("yyyy/MM/dd ").format(date));
		System.out.println(isDateInRange(getPrevNdays(10),getToday(),"yyyy/MM/dd",getPrevNdays(9)));
		
		
		System.out.println(isDateInRange(getToday(), getToday(), YYYYMMDD, "2018/12/05"));
	} 
}
