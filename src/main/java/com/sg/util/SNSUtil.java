package com.sg.util;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;


public class SNSUtil
{
  
    public static void sendSNSMessage(String topicArn, String msg)
    {
        // Get an AWS SNS Client
        AmazonSNS snsClient = new AmazonSNSClient(new DefaultAWSCredentialsProviderChain());
        snsClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
		snsClient.publish(topicArn,msg);
        
         

    }
    
   
}
