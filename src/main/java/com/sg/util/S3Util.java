package com.sg.util;


import java.io.File;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.util.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

public class S3Util {

    static AmazonS3Client amazonS3Client;
    private final static String UTF_8 = "UTF-8";

    private static String S3_PREFIX = "s3://";
    private static Pattern pattern = Pattern.compile("(s3://)(.*?)/");

    private final static Logger logger = LoggerFactory.getLogger(S3Util.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final S3Util s3Util;
    private AWSCredentialsProvider credentialsProvider;

    private static final long MEGABYTE = 1024L * 1024L;

    static {
        s3Util = new S3Util();
    }

    public static S3Util getInstance() {
        return s3Util;
    }

    public AmazonS3 getS3Client() {
        if (amazonS3Client == null) {
            refresh();
        }
        return amazonS3Client;
    }

    /**
     * Made it public to refresh on secret key expiration,
     */
    public void refresh() {
        amazonS3Client = new AmazonS3Client(new DefaultAWSCredentialsProviderChain());
    }


    /**
     * Decode S3 urls to utf-8
     *
     * @param input
     * @return
     */
    public String decode(String input) {
        try {
            return URLDecoder.decode(input, UTF_8);
        } catch (UnsupportedEncodingException e) {
            return input;
        }
    }

    /**
     * Get URI
     *
     * @param bucket
     * @param key
     * @return
     */
    public String uri(String bucket, String key) {
        return String.format("s3://%s/%s", bucket, key);
    }

    /**
     * Get bucket name from s3 url (s3://blahblah/blah/abc.txt
     *
     * @param url
     * @return
     */
    public String getBucket(String url) {
        return url.split("[/]")[2];
    }

    /**
     * Get key Example: "s3://bucket/key/abc.txt" to "key/abc.txt"
     *
     * @param url
     * @return
     */
    public String getKey(String url) {
        String bucket = this.getBucket(url);
        return url.substring(url.indexOf(bucket) + bucket.length() + 1);
    }

 

    /**
     * Uploads a file on the local filesystem to S3
     *
     * @param bucket       S3 bucket to upload the file to
     * @param key          Key where the file will be uploaded
     * @param absolutePath Local file to upload to S3
     * @return the {@link PutObjectResult}
     */
    public PutObjectResult upload(String bucket, String key, String absolutePath) throws AmazonServiceException {
        File file = new File(absolutePath);
        if (StringUtils.isNullOrEmpty(key)) {
            key = file.getName();
        } else if (key.endsWith("/")) {
            key = key + file.getName();
        }
        logger.info(String.format("Uploading %s to %s/%s", absolutePath, bucket, key));
        return this.getS3Client().putObject(bucket, key, file);
    }

    /**
     * Upload content to s3 as an input stream. Useful if you have some generate content
     * and don't want to write it to a local file first just to upload to s3.
     *
     * @param bucket        - the s3 bucket to upload to
     * @param key           - the key (path) of the content to upload
     * @param inputStream   - the inputstream representing the content to upload
     * @param contentLength - the length of the content in the input stream.
     *                      For performance reasons, clients MUST set the content length in the metadata object,
     *                      else the entire input stream will be buffered into memory, with potentially
     *                      dangerous performance implications
     * @param content
     * @return the {@link PutObjectResult} if successful, null if the request params are invalid
     * @throws AmazonServiceException if the request is unsuccessful
     *         mockS3Util.upload("dp-unit-test", "key", IOUtils.toInputStream(testContent), testContent.length());

     */
    public PutObjectResult upload(String bucket, String key, InputStream inputStream, long contentLength) throws AmazonServiceException {

        if (StringUtils.isNullOrEmpty(bucket) || StringUtils.isNullOrEmpty(key) ||
                inputStream == null || contentLength <= 0) {
            logger.warn("Unable to send upload content to s3, invalid input parameters");
            return null;
        }

        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(contentLength);

        return upload(bucket, key, inputStream, metadata);
        
        
    }

    /**
     * Upload content to s3 as an input stream. Useful if you have some generate content
     * and don't want to write it to a local file first just to upload to s3.
     *
     * @param bucket      - the s3 bucket to upload to
     * @param key         - the key (path) of the content to upload
     * @param inputStream - the inputstream representing the content to upload
     * @param metadata    - metadata related to the request. Note for performance reasons, clients
     *                    MUST set the content length in the metadata object, else the entire input stream will
     *                    be buffered into memory, with potentially dangerous performance implications
     * @param content
     * @return the {@link PutObjectResult} if successful, null if the request is invalid
     * @throws AmazonServiceException if the request is unsuccessful
     */
    public PutObjectResult upload(String bucket, String key, InputStream inputStream, ObjectMetadata metadata) throws AmazonServiceException {

        if (StringUtils.isNullOrEmpty(bucket) || StringUtils.isNullOrEmpty(key) ||
                inputStream == null || metadata == null) {
            logger.warn("Unable to send upload content to s3, invalid input parameters");
            return null;
        }

        PutObjectRequest request = new PutObjectRequest(bucket, key, inputStream, metadata);
        return putObject(request);
    }

    /**
     * Makes a put request to the s3 api with the provided request object.
     *
     * @param request
     * @return the {@link PutObjectResult} if successful, null if the request is invalid
     * @throws AmazonServiceException if the request is unsuccessful
     */
    public PutObjectResult putObject(PutObjectRequest request) throws AmazonServiceException {

        if (request == null) {
            logger.warn("Unable to send upload content to s3, put object request was null");
            return null;
        }

        return getS3Client().putObject(request);
    }

  
}

