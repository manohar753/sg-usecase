package com.sg.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SNSNotification {

	private String  alarmName;
	private String  alarmDescription;
	private String  awsAccount;
	private String  newStateValue;
	public String getAlarmName() {
		return alarmName;
	}
	public void setAlarmName(String alarmName) {
		this.alarmName = alarmName;
	}
	public String getAlarmDescription() {
		return alarmDescription;
	}
	public void setAlarmDescription(String alarmDescription) {
		this.alarmDescription = alarmDescription;
	}
	public String getAwsAccount() {
		return awsAccount;
	}
	public void setAwsAccount(String awsAccount) {
		this.awsAccount = awsAccount;
	}
	public String getNewStateValue() {
		return newStateValue;
	}
	public void setNewStateValue(String newStateValue) {
		this.newStateValue = newStateValue;
	}

	public static void main(String[] args) throws JsonProcessingException {
		SNSNotification no=new SNSNotification();
		no.setAlarmDescription("tetingjavaclient");
		no.setAlarmName("Matestalarm");
		no.setAwsAccount("dev");
		no.setNewStateValue("ALARM");
		//Object mapper instance
		ObjectMapper mapper = new ObjectMapper();
		 
		//Convert POJO to JSON
		String json = mapper.writeValueAsString(no);	
		System.out.println(json);
	}
}


